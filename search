#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 10000000

typedef struct {
    char name[51];
    char dob[11];
    char reg_num[7];
    char course_code[5];
    float tuition;
} Student;

// Global array to store student records
Student students[MAX_STUDENTS];
int num_students = 0; // Number of students currently in the array

// Function prototypes
void createStudent();
void printStudent(Student s);
void updateStudent(Student *s);
void deleteStudent(Student *s);
int searchStudentByRegistration(Student students[], int num_students, char registration[]);
void sortStudents(Student students[], int num_students, int field);
void exportStudentsToFile(const char* filename);

int main() {
    int choice;
    do {
        printf("\nMenu:\n");
        printf("1. Create Student\n");
        printf("2. Read Student\n");
        printf("3. Update Student\n");
        printf("4. Delete Student\n");
        printf("5. Search Student by Registration\n");
        printf("6. Search Students\n");
        printf("7. Export Students to File\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        char choice_str[10];
        fgets(choice_str, sizeof(choice_str), stdin);
        choice = atoi(choice_str);

        switch(choice) {
            case 1:
                createStudent();
                break;
            case 2:
                // Read operation
                break;
            case 3:
                // Update operation
                break;
            case 4:
                // Delete operation
                break;
            case 5:
            {
                char reg_num[7];
                printf("Enter registration number to search: ");
                fgets(reg_num, sizeof(reg_num), stdin);
                reg_num[strcspn(reg_num, "\n")] = '\0';
                int index = searchStudentByRegistration(students, num_students, reg_num);
                if (index != -1) {
                    printf("Student found:\n");
                    printStudent(students[index]);
                } else {
                    printf("Student not found.\n");
                }
                break;
            }
            case 6:
                // Search Students
                break;
            case 7:
                exportStudentsToFile("students.csv");
                printf("Students exported to file successfully.\n");
                break;
            case 8:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid choice. Please enter a number from 1 to 8.\n");
        }
    } while(choice != 8);

    return 0;
}

void createStudent() {
    if (num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached.\n");
        return;
    }
    printf("Enter student name: ");
    fgets(students[num_students].name, sizeof(students[num_students].name), stdin);
    students[num_students].name[strcspn(students[num_students].name, "\n")] = '\0';

    printf("Enter date of birth (YYYY-MM-DD): ");
    fgets(students[num_students].dob, sizeof(students[num_students].dob), stdin);
    students[num_students].dob[strcspn(students[num_students].dob, "\n")] = '\0';

    printf("Enter registration number (numeric characters only): ");
    fgets(students[num_students].reg_num, sizeof(students[num_students].reg_num), stdin);
    students[num_students].reg_num[strcspn(students[num_students].reg_num, "\n")] = '\0';

    printf("Enter course code: ");
    fgets(students[num_students].course_code, sizeof(students[num_students].course_code), stdin);
    students[num_students].course_code[strcspn(students[num_students].course_code, "\n")] = '\0';

    printf("Enter annual tuition: ");
    scanf("%f", &students[num_students].tuition);
    getchar(); // Consume newline after scanf

    num_students++; // Increment the number of students
}

void printStudent(Student s) {
    printf("Name: %s\n", s.name);
    printf("Date of Birth: %s\n", s.dob);
    printf("Registration Number: %s\n", s.reg_num);
    printf("Course Code: %s\n", s.course_code);
    printf("Annual Tuition: %.2f\n", s.tuition);
}

int searchStudentByRegistration(Student students[], int num_students, char registration[]) {
    int i;
    for (i = 0; i < num_students; i++) {
        if (strcmp(students[i].reg_num, registration) == 0) {
            return i;
        }
    }
    return -1;
}

void exportStudentsToFile(const char* filename) {
    FILE *file = fopen(filename, "w"); // Open file in write mode

    if (file == NULL) {
        printf("Error opening file for writing.\n");
        return;
    }

    // Write header to the file
    fprintf(file, "Name,Date of Birth,Registration Number,Course Code,Annual Tuition\n");

    // Write student records to the file
    for (int i = 0; i < num_students; ++i) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dob, students[i].reg_num, students[i].course_code, students[i].tuition);
    }

    fclose(file);
}
