void sortStudents(Student students[], int num_students, int field) {
    // Field 1: Sort by name, Field 2: Sort by registration number
    switch(field) {
        case 1:
            // Sort by name
            for (int i = 0; i < num_students - 1; i++) {
                for (int j = 0; j < num_students - i - 1; j++) {
                    if (strcmp(students[j].name, students[j + 1].name) > 0) {
                        // Swap
                        Student temp = students[j];
                        students[j] = students[j + 1];
                        students[j + 1] = temp;
                    }
                }
            }
            printf("Students sorted by name.\n");
            break;
        case 2:
            // Sort by registration number
            for (int i = 0; i < num_students - 1; i++) {
                for (int j = 0; j < num_students - i - 1; j++) {
                    if (strcmp(students[j].reg_num, students[j + 1].reg_num) > 0) {
                        // Swap
                        Student temp = students[j];
                        students[j] = students[j + 1];
                        students[j + 1] = temp;
                    }
                }
            }
            printf("Students sorted by registration number.\n");
            break;
        default:
            printf("Invalid field.\n");
    }
}
