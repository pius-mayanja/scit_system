#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Structure to store student information
struct Student {
    char name[50];
    char dob[11]; // Date of birth format: YYYY-MM-DD
    char reg_number[15];
    char course_code[10];
    char program[20];
    float tuition;
    struct Student *next;
};

// Function prototypes
void displayMenu();
void createStudent();
void readStudents();
void updateStudent();
void deleteStudent();
void searchStudent();
void clearBuffer();

// Global variable for the linked list head
struct Student *head = NULL;

int main() {
    int choice;

    do {
        displayMenu();
        printf("Enter your choice: ");
        scanf("%d", &choice);
        clearBuffer();

        switch (choice) {
            case 1:
                createStudent();
                break;
            case 2:
                readStudents();
                break;
            case 3:
                updateStudent();
                break;
            case 4:
                deleteStudent();
                break;
            case 5:
                searchStudent();
                break;
            case 6:
                printf("Exiting program...\n");
                break;
            default:
                printf("Invalid choice! Please enter a number between 1 and 6.\n");
        }
    } while (choice != 6);

    return 0;
}

// Function to display menu options
void displayMenu() {
    printf("\n=== Student Record Management System ===\n");
    printf("1. Create a new student record\n");
    printf("2. Read student records\n");
    printf("3. Update a student record\n");
    printf("4. Delete a student record\n");
    printf("5. Search for a student record\n");
    printf("6. Exit\n");
}

// Function to create a new student record
void createStudent() {
    struct Student *newStudent = (struct Student*)malloc(sizeof(struct Student));
    printf("\nEnter student details:\n");
    printf("Name: ");
    fgets(newStudent->name, sizeof(newStudent->name), stdin);
    printf("Date of Birth (YYYY-MM-DD): ");
    fgets(newStudent->dob, sizeof(newStudent->dob), stdin);

    // Read and store registration number separately
    char reg_num_input[50];
    printf("Registration Number: ");
    fgets(reg_num_input, sizeof(reg_num_input), stdin);
    sscanf(reg_num_input, "%*[^:]:%[^/]", newStudent->reg_number);

    printf("Course Code: ");
    fgets(newStudent->course_code, sizeof(newStudent->course_code), stdin);
    printf("Program: ");
    fgets(newStudent->program, sizeof(newStudent->program), stdin);
    printf("Annual Tuition: ");
    scanf("%f", &newStudent->tuition);
    clearBuffer();

    newStudent->next = head;
    head = newStudent;
    printf("Student record created successfully!\n");
}



// Function to read and display all student records
void readStudents() {
    if (head == NULL) {
        printf("No student records found!\n");
        return;
    }

    struct Student *current = head;
    int count = 1;

    printf("\n=== Student Records ===\n");
    while (current != NULL) {
        printf("Student %d:\n", count);
        printf("Name: %s", current->name);
        printf("Date of Birth: %s", current->dob);
        printf("Registration Number: %s\n", current->reg_number); // Adjusted output format
        printf("Course Code: %s", current->course_code);
        printf("Program: %s", current->program);
        printf("Annual Tuition: $%.2f\n", current->tuition);
        current = current->next;
        count++;
    }
}

// Function to update a student record
void updateStudent() {
    printf("Update a student record functionality is not yet implemented.\n");
}

// Function to delete a student record
void deleteStudent() {
    printf("Delete a student record functionality is not yet implemented.\n");
}

// Function to search for a student record by registration number
void searchStudent() {
    char search_reg_number[15];
    printf("Enter registration number to search: ");
    fgets(search_reg_number, sizeof(search_reg_number), stdin);
    // Remove trailing newline character
    search_reg_number[strcspn(search_reg_number, "\n")] = '\0';

    struct Student *current = head;
    int found = 0;
    int count = 1;

    while (current != NULL) {
        if (strcmp(current->reg_number, search_reg_number) == 0) {
            printf("Student found at position %d:\n", count);
            printf("Name: %s\n", current->name);
            printf("Date of Birth: %s\n", current->dob);
            printf("Registration Number: %s\n", current->reg_number);
            printf("Course Code: %s\n", current->course_code);
            printf("Program: %s\n", current->program);
            printf("Annual Tuition: $%.2f\n", current->tuition);
            found = 1;
            break;
        }
        current = current->next;
        count++;
    }

    if (!found) {
        printf("Student with registration number %s not found.\n", search_reg_number);
    }
}


// Function to clear input buffer
void clearBuffer() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}
